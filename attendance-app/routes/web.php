<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentCtrl;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [StudentCtrl::class, 'getStudents'])->name('students');
Route::post('/', [StudentCtrl::class,'delStudent']);
Route::get('/addStudent', function (){
    return view('addStudent');
})->name('addStudent');
Route::post('/addStudent', [StudentCtrl::class,'ajoutStudent']);

Route::get('/horaire', [StudentCtrl::class, 'getHoraire']);