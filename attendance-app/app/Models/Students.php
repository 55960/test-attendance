<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
class Students {

    public static function getStudents(){
        $result = DB::select("SELECT matricule, nom, prenom from student order by matricule");
        return $result;
    }

    public static function getHoraire(){
        return DB::table('horaire')
        ->select('cour', 'date', 'heure')
        ->orderBy('date')
        ->orderBy('heure')
        ->get();
    }

    public static function cour($courId){
        return DB::table('horaire')
            ->select('cour', 'date', 'heure')
            ->where('cour', $courId)
            ->get();
    }

    public static function deleteStudent($matricule){
        return DB::table('student')
            ->where('matricule', $matricule)
            ->delete();
    }

    public static function ajoutStudent($matricule,$nom,$prenom){
        if ($matricule < 0) {
            throw new \PDOException("Le matricule $matricule est négatif.");
        }
            
        $existingStudent = DB::table('student')->where('matricule', $matricule)->first();

        if ($existingStudent) {
            throw new \PDOException("Un étudiant avec le matricule $matricule existe déjà.");
        }
        return DB::insert('insert into student (`Matricule`, `nom`, `prenom`) values (?, ?, ?)', [$matricule,$nom,$prenom]);
    
    }

    public static function updateStudent($matricule, $nom, $prenom){
        return DB::update("UPDATE Student set nom = ?, prenom = ? where matricule = ?", [$nom, $prenom, $matricule]);
    }
 
}