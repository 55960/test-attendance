<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Illuminate\Support\Facades\DB;

use App\Models\Students;
class StudentTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     */
    public function testExample(): void
    {
        $this->browse(function (Browser $browser) {

            $browser->visit('/')
                ->assertSee('Liste des présences');


                $browser->assertSee('Matricule')
                ->assertSee('Nom')
                ->assertSee('Prénom');

        });
    }

    public function test_student_matricule__is_Sorted()
    {
        $this->browse(function (Browser $browser) {

            $studentsFromDatabase = DB::table('student')->orderBy('matricule')->pluck('matricule')->toArray();
        
            $browser->visit('/')
                ->waitForText('Liste des présences'); 
        
            $matriculesOnPage = $browser->elements('.matricule'); 
            $matriculesOnPageArray = [];
            
            foreach ($matriculesOnPage as $element) {
                $matriculesOnPageArray[] = $element->getText();
            }
        
            $this->assertEquals($studentsFromDatabase, $matriculesOnPageArray);
        });
    }

    public function test_insert(){
        $this->browse(function (Browser $browser){
            
            $browser->visit('/addStudent');

            $browser->type('matricule', 78947);
            $browser->type('nom', 'Test');
            $browser->type('prenom', 'Patrick');
            
            $browser->press('envoyer');
                        
            $browser->visit('/')->assertSee('Patrick');
            Students::deleteStudent(78947);
        });
    }

    public function test_delete(){
        $this->browse(function (Browser $browser){
            $browser->visit('/');

            Students::ajoutStudent(88888, 'Test', 'Karim');
            Students::deleteStudent(88888);
            $browser->assertDontSee('Test Karim');
        });
    }



    }

    

