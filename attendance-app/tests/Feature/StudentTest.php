<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Students;

class StudentTest extends TestCase
{

    use RefreshDatabase;

    public function test_insert_data()
    {
        $matricule = 59858;
        $nom = 'SpongeBob';
        $prenom = 'SquarePants';

        Students::ajoutStudent($matricule, $nom, $prenom);

        $this->assertDatabaseHas('student', [
            'matricule' => $matricule,
            'nom' => $nom,
            'prenom' => $prenom,
        ]);

    }

    public function test_DeleteData()
    {
        $matricule = 57844;
        $nom = 'SpongeBob';
        $prenom = 'SquarePants';

        Students::ajoutStudent($matricule, $nom, $prenom);
        Students::deleteStudent($matricule);

        $this->assertDatabaseMissing('student', [
            'matricule' => $matricule,
            'nom' => $nom,
            'prenom' => $prenom,
        ]);

    }

    public function test_insert_student_with_wegative_watricule()
    {
        $this->expectException(\PDOException::class);
        Students::ajoutStudent(-1, 'SpongeBob', 'SquarePants');
    }

    public function test_when_adding_a_student_with_existing_number()
    {
        $matricule = 1;
        Students::ajoutStudent($matricule, 'SpongeBob', 'SquarePants');

        $this->expectException(\PDOException::class);

        Students::ajoutStudent($matricule, 'SpongeBob', 'SquarePants');
    }

    public function test_update_student(){

        Students::ajoutStudent(56666, 'SpongeBob', 'SquarePant');
        Students::updateStudent(56666, 'Patrick', 'Star');
        
        $this->assertDatabaseHas('student', [
            'matricule' => 56666,
            'nom' => 'Patrick',
            'prenom' => 'Star',
        ]);
    }
}

