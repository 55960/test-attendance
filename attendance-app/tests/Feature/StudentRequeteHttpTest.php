<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StudentRequeteHttpTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_consult_all_student(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_consult_one_student(){
   
        $response = $this->get('/',['matricule' => 69747]);
        $response->assertStatus(200);

   }


    public function test_create_student(){
        $response = $this->post('/addStudent', [

            'matricule' => 56682,

            'nom' => 'SpongeBob',

            'prenom' => 'SquarePant',

        ]);

        $response->assertStatus(302);
    }

    public function test_remove_student(){

        $this->post('/addStudent', [
           'matricule' => 78787,

           'nom' => 'SquarePants',

           'prenom' => 'SpongBob',
       ]);

       $response = $this->post('/', ['matricule' => 55555]);

       $response->assertStatus(302);
   }


   


}
