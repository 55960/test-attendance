<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('student')->insert([
            ['Matricule' => 68393, 'nom' => 'Florian ', 'prenom' => 'Barthet'],
            ['Matricule' => 51111, 'nom' => 'Tristan ', 'prenom' => 'Bessette'],
            ['Matricule' => 66457, 'nom' => 'Damien  ', 'prenom' => 'Picard'],
            ['Matricule' => 60288, 'nom' => 'Fernand  ', 'prenom' => 'Bourguignon'],
            ['Matricule' => 63696, 'nom' => 'Jean-Michel ', 'prenom' => 'Joubert'],
            ['Matricule' => 64574, 'nom' => 'Trottier ', 'prenom' => 'Matthias '],
            ['Matricule' => 58575, 'nom' => 'Grandjean ', 'prenom' => 'Bessette'],
            ['Matricule' => 56883, 'nom' => 'Mathieu  ', 'prenom' => 'Mathieu '],
            ['Matricule' => 68342, 'nom' => 'Chéreau ', 'prenom' => 'David '],
            ['Matricule' => 69747, 'nom' => 'Schaeffer  ', 'prenom' => 'Claude '],
            ['Matricule' => 67446, 'nom' => 'LeMahieu ', 'prenom' => 'Dylan '],
            ['Matricule' => 53804, 'nom' => 'Longchambon ', 'prenom' => 'Abélia '],
            ['Matricule' => 50037, 'nom' => 'Manaudou ', 'prenom' => 'Adélie '],
            ['Matricule' => 58701, 'nom' => 'Christelle  ', 'prenom' => 'Gueguen'],
            ['Matricule' => 64630, 'nom' => 'LeBeau ', 'prenom' => 'Leila ']
            
        ]);
    }
}
