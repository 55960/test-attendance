<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HoraireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('horaire')->insert([
            ['cour' => 'PRJG5', 'date' => '2023-09-25', 'heure' => '10:30'],
            ['cour' => 'VEI5', 'date' => '2023-09-22', 'heure' => '10:30'],
            ['cour' => 'TEX5', 'date' => '2023-09-21', 'heure' => '10:30'],
            ['cour' => 'ORG-G5', 'date' => '2023-09-20', 'heure' => '10:30'],
            ['cour' => 'MOBG5', 'date' => '2023-09-19', 'heure' => '10:30'],
            
        ]);
    }
}
