@extends('canevas')
@section('title', 'etudiant ajout')
@section('title_header', 'Ajout etudiant')
@section('content')
    <form action="/addStudent" method="post" class="student">
        {{csrf_field()}}
        <label for="matricule">Entrez votre Matricule:</label>
        <input type="number" name="matricule" min="50000" required><br>
        <label for="nom">Entrez votre nom: </label>
        <input type="text" name="nom"  required /><br>
        <label for="prenom">Entre votre prenom: </label>
        <input type="text" name="prenom"  required /><br>
        <input type="submit" value="envoyer" class="student-button">
    </form>
@endsection