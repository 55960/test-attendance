@extends('canevas')
@section('title', 'Horaire')
@section('title_header', 'Liste des cours')
@section('content')

<table>
    <th>
        Cour
    </th>
    <th>
        Date
    </th>
    <th>
        Heure
    </th>
    @foreach($horaire as $item)
    <tr>
        <td>
            {{$item->cour}}
        </td>
        <td>
            {{$item->date}}
        </td>
        <td>
            {{$item->heure}}
        </td>
        
    </tr>
    @endforeach
</table>
@endsection